
CREATE DATABASE scraper;

USE scraper;

CREATE TABLE queue1 ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64),
    ok INT )
    ENGINE = innodb;

CREATE INDEX queue1_trans on queue1(trans);

CREATE TABLE queue1_pend ( 
    id INT NOT NULL PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) NOT NULL )
    ENGINE = innodb;

CREATE INDEX queue1_pend_trans on queue1_pend(trans);

CREATE TABLE queue2 ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64),
    ok INT )
    ENGINE = innodb;

CREATE INDEX queue2_trans on queue2(trans);

CREATE TABLE queue2_pend ( 
    id INT NOT NULL PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) NOT NULL )
    ENGINE = innodb;

CREATE INDEX queue2_pend_trans on queue2_pend(trans);

CREATE TABLE queue3 ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64),
    ok INT )
    ENGINE = innodb;

CREATE INDEX queue3_trans on queue3(trans);

CREATE TABLE queue3_pend ( 
    id INT NOT NULL PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) NOT NULL )
    ENGINE = innodb;

CREATE INDEX queue3_pend_trans on queue3_pend(trans);

CREATE TABLE queue4 ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64),
    ok INT )
    ENGINE = innodb;

CREATE INDEX queue4_trans on queue4(trans);

CREATE TABLE queue4_pend ( 
    id INT NOT NULL PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) NOT NULL )
    ENGINE = innodb;

CREATE INDEX queue4_pend_trans on queue4_pend(trans);

CREATE TABLE queue5 ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL )
    ENGINE = innodb;

CREATE TABLE queue5_pend ( 
    id INT NOT NULL PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) NOT NULL )
    ENGINE = innodb;

CREATE INDEX queue5_pend_trans on queue5_pend(trans);

CREATE DATABASE message;

USE message;

CREATE TABLE ads ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    product VARCHAR(256) NULL,
    state VARCHAR(256) NULL,
    city VARCHAR(256) NULL,
    category VARCHAR(256) NULL,
    name VARCHAR(256) NULL,
    phone VARCHAR(256) NULL,
    url VARCHAR(1024) NULL )
    ENGINE = innodb;

CREATE INDEX ads_name on ads(name);

CREATE INDEX ads_product on ads(product);

CREATE TABLE queue ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) )
    ENGINE = innodb;

CREATE INDEX queue_trans on queue(trans);

CREATE TABLE sent ( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(2048) NOT NULL,
    trans VARCHAR(64) )
    ENGINE = innodb;

CREATE INDEX sent_trans on sent(trans);

