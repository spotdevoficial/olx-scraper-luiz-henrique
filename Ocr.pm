#!/usr/bin/env perl
use 5.016;
use open qw(:locale);
use strict;
use utf8;
use utf8::all;
use Encode 'encode';
use warnings qw(all);

use Timer;

sub get_ocr {
    my ($img, $timer) = @_;

    my $conf = 'tess_conf';
    `echo 'tessedit_char_whitelist 0123456789()' > $conf`
        unless -f $conf;

    $timer->measure("ocr_convert");
    `convert -filter Lanczos -resample 300x300 -enhance $img ${img}.out`;
    $timer->stop();

    $timer->measure("ocr_tesseract");
    my $out = `tesseract ${img}.out - nobatch tess_conf`;
    $timer->stop();

    return $out =~ /([0123456789() ]+)/ ? $1 : '';
}

1;
