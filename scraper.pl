#!/usr/bin/env perl
use 5.016;
use open qw(:locale);
use strict;
use utf8;
use utf8::all;
use Encode 'encode';
use warnings qw(all);

use Carp qw(croak carp);
use String::Util qw(trim);
use Mojo::UserAgent;
use Mojo::JSON qw(decode_json encode_json);
use File::Temp;
use Data::Dumper;
use DBD::mysql;
use Try::Tiny;
use Ocr;
use Queue;
use Timer;
use config;

# Usage:
# start scavenging everything
# perl scraper.pl start
# start scavenging with some filters
# perl scraper.pl start [state] [category] [page_min] [page_max]
# don't run the main loop, just enqueue new filters on the database
# perl scraper.pl queue [state] [category] [page_min] [page_max]
# continue scavenging until queue empties
# perl scraper.pl
# continue scavenging forever, used to run more processes
# perl scraper.pl loop

# Command line parameters
my ($mode, $filter_state, $filter_category, $filter_page_min, $filter_page_max) = @ARGV;

$mode = '' unless $mode;
$filter_state = '.+' unless $filter_state;
$filter_category = '.+' unless $filter_category;
$filter_page_min = 0 unless $filter_page_min;
$filter_page_max = 99999999 unless $filter_page_max;

my $filter = {
    state => $filter_state,
    category => $filter_category,
    page_min => $filter_page_min,
    page_max => $filter_page_max
};

# Consts
my $step_max = 5;

# Database
my $db_host = config()->{host};
my $db_user = config()->{user};
my $db_pass = config()->{pass};

my $urls = {};

for my $step ( 1 .. $step_max ) {
    $urls->{$step} = Queue->create({
        host => $db_host,
        db => 'scraper',
        table => 'queue' . $step,
        user => $db_user,
        pass => $db_pass
    });
}

my $messages = Queue->create({
    host => $db_host,
    db => 'message',
    table => 'queue',
    user => $db_user,
    pass => $db_pass
});

my $url_db = {};
my $product_db = {};

unless ( $messages->{dbi}->prepare("
        SHOW TABLES LIKE 'ads';
    ")->execute() > 0 ) {
    $messages->{dbi}->prepare("
        CREATE TABLE ads (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            product VARCHAR(256) NULL,
            state VARCHAR(256) NULL,
            city VARCHAR(256) NULL,
            category VARCHAR(256) NULL,
            name VARCHAR(256) NULL,
            phone VARCHAR(256) NULL )
            ENGINE = innodb;
    ")->execute();
}

# Push initial link
if ( $mode eq 'start' or $mode eq 'queue' ) {
    $urls->{1}->push({ url => 'http://www.olx.com.br/', filter => $filter });
    exit if $mode eq 'queue';
}

# Service mode
my $forever = $mode eq 'loop';

# User agent
my $ua = Mojo::UserAgent
    ->new(
        max_redirects => 5,
        max_connections => 16, 
        connect_timeout => 100,
        request_timeout => 200,
        inactivity_timeout => 400);

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

#$ua->cookie_jar(Mojo::UserAgent::CookieJar->new());

# Limit parallel connections
my $max_conn = 8;

# Keep track of active connections
my $active = 0;

# Steps
my $steps = {
    1 => \&parse_html_step1,
    2 => \&parse_html_step2,
    3 => \&parse_html_step3,
    4 => \&parse_html_step4,
    5 => \&parse_html_step5
};

# Main Loop
Mojo::IOLoop->recurring(1 => sub {
    for ( $active + 1 .. $max_conn ) {
    for my $step ( 1 .. $step_max ) {
        last if $active + 1 > $max_conn;

        # Timing
        my $timer = Timer->create();
        $timer->measure("pagination") if $step == 4;
        $timer->measure("ad_download") if $step == 5;

        # Dequeue
        $timer->measure("queue_pop");
        next unless my $run = $urls->{$step}->pop();
        $timer->stop();

        say $$ . ' ' . time . ' ' . $active . '-' . $step;

        # Fetch non-blocking just by adding
        # a callback and marking as active
        ++$active;

        # Do the asynchonous get
        $ua->get($run->{url} => sub {
            my ($self, $tx) = @_;
            try {

                # Run the real callback
                my $ret = do_callback(
                    $tx,
                    $steps->{$step},
                    $run->{filter},
                    $timer);

                # Mark as finished with status
                $timer->measure("queue_done");
                $urls->{$step}->done($run, $ret);
                $timer->stop();

                # Show time report
                if ( $step >= 4 and $ret and $ret == 1 ) {
                    $timer->stop();
                    $timer->report();
                }

            } catch {
                my $error = $_;

                # Mark as finished with error
                $urls->{$step}->done($run, 0);

                # Display error                
                carp Dumper($error);

            };

            # Deactivate
            --$active;

        });
    }}
});

# Watcher, used to stop the process when finished
Mojo::IOLoop->recurring(
    60 => sub {

        # count the queues
        my $any_active = $active;
        for my $step ( 1 .. $step_max ) {
            $any_active += $urls->{$step}->peek();
        }

        # or halt if there are no active crawlers anymore
        Mojo::IOLoop->stop() if not $any_active and not $forever;

    }
);

# Start event loop if necessary
Mojo::IOLoop->start() unless Mojo::IOLoop->is_running;

sub do_callback {
    my ($tx, $cb, $filter, $timer) = @_;

    # Handle errors
    carp Dumper($tx->error) unless $tx->success;

    # Parse only OK responses
    return 0 if not $tx->res->is_status_class(200);

    # Request URL
    my $url = $tx->req->url;
    my $surl = $url->to_string();

    # Access every link only once
    $timer->measure("url_db_get");
    return $url_db->{$surl}
        if $url_db->{$surl};
    $timer->stop();

    # Specific callback
    $timer->measure("callback");
    my $ret = $cb->($url, $filter, $tx, $timer);
    $timer->stop();

    # Save the link in memory
    $timer->measure("url_db_put");
    $url_db->{$surl} = $ret;
    $timer->stop();

    return $ret;
}

sub parse_html_links {
    my ($url, $tx, $section, $elem, $filter, $callback) = @_;

    # Parse only HTML responses
    return if $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Extract list of links
    my $sec = $tx->res->dom($section)->first;

    # Extract and enqueue URLs
    for my $e ($sec->find($elem)->each) {

        # ignore javascript links
        my $href = $e->{href};
        next unless $href;

        # Validate href attribute
        my $link = Mojo::URL->new($href);
        next if 'Mojo::URL' ne ref $link;

        # "normalize" link
        $link = $link->to_abs($tx->req->url)->fragment(undef);
        next unless grep { $link->protocol eq $_ } qw(http https);

        # Filter
        next unless grep { $link->to_string() =~ $_ } $filter;

        # Process the link
        $callback->($link, $e);
    }

    return;
}

sub parse_html_step1 {
    my ($url, $filter, $tx, $timer) = @_;

    #say "step1 $url";

    parse_html_links($url, $tx,
        'div[class~=section_state]',
        'a[class~=link]',
        $filter->{state},
        sub {
            my ($link, $e) = @_;
            # Don't visit our parent
            return if $link->host eq $url->host;
            # Push the link
            $urls->{2}->push({ url => $link, filter => $filter });
            #say " -> $link";
        });

    return 1;
}

sub parse_html_step2 {
    my ($url, $filter, $tx, $timer) = @_;

    #say "step2 $url";

    parse_html_links($url, $tx,
        'div[class~=search-category]',
        'a[class~=link]',
        $filter->{category},
        sub {
            my ($link, $e) = @_;
            $link = $link . "?f=c";
            # Push the link
            $urls->{3}->push({ url => $link, filter => $filter });
            #say "    -> $link";
        });

    return 1;
}

sub parse_html_step3 {
    my ($url, $filter, $tx, $timer) = @_;

    #say "step3 $url";

    my $min = $filter->{page_min};
    my $max = 0;
    my $reflink;

    parse_html_links($url, $tx,
        'div[class~=module_pagination]',
        'a[class~=link]',
        '.+',
        sub {
            my ($link, $e) = @_;
            my $pg = $link =~ /(\d+)$/ ? int($1) : 0;
            if ( $pg > $max ) { $max = $pg; $reflink = $link; }
        });

    if ( $max > $filter->{page_max} ) { $max = $filter->{page_max}; }

    $reflink = $reflink =~ /^([^\d]+)\d+$/ ? $1 : '';

    # Push the links
    for my $page ( $min .. $max ) {
        my $link = $reflink . $page;
        $urls->{4}->push({ url => $link, filter => $filter });
        #say "        -> $link";
    }

    return 1;
}

sub parse_html_step4 {
    my ($url, $filter, $tx, $timer) = @_;

    #say "step4 $url";

    parse_html_links($url, $tx,
        'div[class~=section_OLXad-list]',
        'a[class~=OLXad-list-link]',
        '.+',
        sub {
            my ($link, $e) = @_;

            my $tx;
            my $ok = 0;
            until ($ok) {

                # Download the ad
                $timer->measure("ad_download");
                $tx = $ua->get($link);
                $timer->stop();            

                # Handle errors
                $ok = $tx->success;
                carp Dumper($tx->error) unless $ok;
            }

            # Parse only OK responses
            return if not $tx->res->is_status_class(200);

            # Process the ad
            $ok = parse_html_step5($link, $filter, $tx, $timer);

            # There was an error
            $urls->{5}->push({ url => $link, filter => $filter })
                if not $ok or $ok <= 0;

            # Show time report
            if ( $ok and $ok == 1 ) {
                $timer->report();
            }

            #say "            -> $link";
        });

    return 1;
}

sub parse_html_step5 {
    my ($url, $filter, $tx, $timer) = @_;

    #say "step5 $url";

    # Parse only HTML responses
    return 2 if $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # View
    my $view = $tx->res->dom()->at('div[class~=page_OLXad-view]');

    # Main ad data
    my $ad = $view->at('div[class~=section_OLXad-info]') if $view;

    # Extract ad data
    my $id = $ad->at('div[class~=OLXad-id]') if $ad;
    my $id_desc = $id->at('strong[class~=description]') if $id;
    return 2 unless $id_desc;

    # Filter ad id
    my $ad_id = $id_desc->all_text() if $id_desc;

    # Extract ad name
    my $title = $view->at('h1[class~=OLXad-title]');

    # Filter product
    my $ad_product = $title->all_text() if $title;

    # Extract ad location
    my $city;
    my $loc = $ad->at('div[class~=OLXad-location]');
    $loc = $ad->at('div[class~=OLXad-location-map]') unless $loc;
    if ($loc) {
        for my $e ($loc->find('li[class~=item]')->each) {
            my $term = $e->at('span[class~=term]');
            next unless $term and $term->all_text() =~ /munic.pio/i;
            $city = $e->at('strong[class~=description]');
        }
    }

    # Filter city
    my $ad_city = $city->all_text() if $loc and $city;

    # Extract ad category
    my $category;
    my $categ = $view->at('div[class~=module_breadcrumb]');
    if ($categ) {
        my $ix = 0;
        for my $e ($categ->find('li[class~=item]')->each) {
            my $lnk = $e->at('a[class~=link]');
            next unless $lnk;
            last if $ix++ >= 3;
            $category = $lnk->{href};
        }
    }

    # Filter category
    my $ad_category = $category =~
        /^[^:]+[:]\/\/[^.]+[.]olx[.][^\/]+\/[^\/]+\/([^\/?]+)/
        ? $1 : '';

    # Extract user data
    my $user = $view->at('div[class~=section_OLXad-user-info]');
    my $name = $user->at('li[class~=owner]') if $user;
    my $phone = $user->at('li[class~=phone]') if $user;
    my $imgphone = $phone->at('img[class~=number]') if $phone;

    # Filter name
    my $user_name = $name->all_text() if $name;

    # Filter phone
    my $link = Mojo::URL->new($imgphone->{src}) if $imgphone;
    $link = $link->to_abs($tx->req->url)->fragment(undef)
        unless not $link or 'Mojo::URL' ne ref $link;

    # Do the OCR
    $timer->measure("ocr_total");
    my $user_phone = ocr_image($link, $ad_id, $timer) if $link;
    $timer->stop();

    # Save Ad
    my $ok = insert_ad({
        id => $ad_id,
        product => $ad_product,
        city => $ad_city,
        category => $ad_category,
        user_name => $user_name,
        user_phone => $user_phone,
        url => $url->to_string(),
        filter => $filter
    }, $timer);

    #say "            ? $url";

    return $ok;
}

sub ocr_image {
    my ($link, $ad_id, $timer) = @_;

    #say "step6 $link";

    # Download the image
    $timer->measure("ocr_download");
    my $tx = $ua->get($link);
    $timer->stop();

    # Handle errors
    carp Dumper($tx->error) unless $tx->success;

    # Parse only OK responses
    return if not $tx->res->is_status_class(200);

    # Detect image type
    my $type = $tx->res->headers->content_type =~ /^image\/(.+)$/ ? $1 : '';

    # Grab the image data
    $timer->measure("ocr_move");
    my $image = '/var/ramfs/tmp/' . $ad_id . '.' . $type;
    $tx->res->content->asset->move_to($image);
    $timer->stop();

    # Scan image
    my $string = get_ocr($image, $timer);

    # Delete the image
    $timer->measure("ocr_delete");
    unlink $image;

    # Delete temporary files, (usually a tiff and a txt)
    unlink glob ( $image . '.*' );
    $timer->stop();

    # Save the image
    my $user_phone = trim $string;

    return $user_phone;
}

sub exists_ad_product {
    my ($product, $timer) = @_;

    $timer->measure("exists_ad_prod");

    $product = encode('ISO-8859-1', $product);
    $product = '' unless $product;

    # Access every product only once
    $timer->measure("product_db_get");
    return 1 if $product_db->{$product};
    $timer->stop();

    # verify if the product doesn't already exists
    my $fetch = $messages->{dbi}->prepare_cached("
        SELECT id FROM ads
        WHERE id = ?;
    ");
    $fetch->execute($product);
    my $row = $fetch->fetchall_arrayref([0])->[0]->[0];

    # Save the product in memory
    if ($row) {
        $timer->measure("product_db_put");
        $product_db->{$product} = 1;
        $timer->stop();
    }

    $timer->stop();

    return 1 if $row;
}

sub insert_ad {
    my ($ad, $timer) = @_;

    $timer->measure("insert_ad");

    #say Dumper($ad);

    # Extract data
    my $url = $ad->{url};
    my $filter = $ad->{filter};
    my $id = $ad->{id};
    my $product = $ad->{product};
    my $state = $url =~ /^[^:]+[:]\/\/(.+)[.]olx[^\/]+\// ? $1 : '';
    my $category = $ad->{category};
    my $city = $ad->{city};
    my $user_name = $ad->{user_name};
    my $user_phone = $ad->{user_phone};

    # Don't push if product alread exists
    return 2 if exists_ad_product($id, $timer);

    # convert encoding
    $category =~ s/-/ /g;
    $product = encode('ISO-8859-1', $product);
    $city = encode('ISO-8859-1', $city);
    $category = encode('ISO-8859-1', $category);
    $user_name = encode('ISO-8859-1', $user_name);
    $user_phone = encode('ISO-8859-1', $user_phone);

    # save to ad table
    $messages->{dbi}->prepare_cached("
        INSERT INTO ads ( id, product, state, city, category, name, phone )
        VALUES ( ?, ?, ?, ?, ?, ?, ? );
    ")->execute($id, $product, $state, $city, $category, $user_name, $user_phone);

    # push to message.pl
    $messages->push({
        obj => {
            product => int($id),
            message => $filter->{message},
            filter => $filter,
            url => $url}
    });

    # cache
    $timer->measure("product_db_put");
    $product_db->{$id} = 1;
    $timer->stop();
    
    return 1;
}
