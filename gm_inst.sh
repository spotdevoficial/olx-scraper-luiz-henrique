
apt-get update
apt-get install build-essential checkinstall libx11-dev libxext-dev zlib1g-dev libpng12-dev libjpeg-dev libfreetype6-dev libxml2-dev
apt-get build-dep imagemagick

wget http://www.imagemagick.org/download/ImageMagick.tar.gz
tar -xzvf ImageMagick.tar.gz
cd ImageMagick-6.9.2-10
./configure
make -j 8
make install

ln -L /usr/local/bin/convert /usr/bin/convert
