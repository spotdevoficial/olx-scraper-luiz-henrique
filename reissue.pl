#!/usr/bin/env perl
use 5.016;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json);
use Queue;
use config;

# Usage:
# Set all requests that don't have response as not processed
# perl reissue.pl

# Consts
my $step_max = 5;

# Database
my $queue_host = config()->{host};
my $queue_user = config()->{user};
my $queue_pass = config()->{pass};

my $urls = {};

for my $step ( 1 .. $step_max ) {

    $urls->{$step} = Queue->create({
        host => $queue_host,
        db => 'scraper',
        table => 'queue' . $step,
        user => $queue_user,
        pass => $queue_pass,
        batch => 1
    });

    my $queue = 'queue' . $step;

    try { $urls->{$step}->{dbi}->prepare("
        update $queue set trans = null
        where trans is not null and ok is null;
    ")->execute(); };

}
