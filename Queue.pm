#!/usr/bin/env perl
package Queue {
    use 5.016;
    use open qw(:locale);
    use strict;
    use utf8;
    use warnings qw(all);

    use Mojo::JSON qw(decode_json encode_json);
    use Data::Dumper;
    use DBI;

    sub create {
        my $class = shift;
        my $self = bless {}, $class;
        my ($p) = @_;

        my $dsn = 'dbi:mysql:mysql;host=' . $p->{host};
        my $db = $p->{db};
        my $user = $p->{user};
        my $pass = $p->{pass};
        my $table = $p->{table};
        my $op = { autocommit => 1 };

        my $dbi = DBI->connect( $dsn, $user, $pass, $op );

        unless ( $dbi->prepare("
                SHOW DATABASES LIKE '$db';
            ")->execute() > 0 ) {
            $dbi->prepare("
                CREATE DATABASE $db;
            ")->execute();
        }

        $dbi->prepare("
            USE $db;
        ")->execute();

        $dbi->prepare("
            SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
        ")->execute();

        unless ( $dbi->prepare("
                SHOW TABLES LIKE '$table';
            ")->execute() > 0 ) {
            $dbi->prepare("
                CREATE TABLE $table (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    value LONGBLOB NOT NULL,
                    trans VARCHAR(64),
                    ok INT )
                    ENGINE = innodb;
            ")->execute();
            $dbi->prepare("
                CREATE INDEX $table\_trans on $table(trans);
            ")->execute();
            $dbi->prepare("
                CREATE INDEX $table\_ok on $table(ok);
            ")->execute();
            $dbi->prepare("
                CREATE INDEX $table\_trans_ok on $table(trans, ok);
            ")->execute();
        }

        $self->{dbi} = $dbi;
        $self->{table} = $table;
        return $self;
    }

    sub add {
        my ($self, $id, $obj) = @_;
        my $vl = encode_json $obj;
        my $dbi = $self->{dbi};
        my $queue = $self->{table};

        $dbi->prepare_cached("
            INSERT INTO $queue ( id, value ) VALUES ( ?, ? );
        ")->execute($id, $vl);

    }

    sub peek {
        my ($self) = @_;
        my $dbi = $self->{dbi};
        my $queue = $self->{table};

        my $fetch = $dbi->prepare_cached("
            SELECT id FROM $queue WHERE trans IS NULL LIMIT 1;
        ");
        $fetch->execute();
        my $row = $fetch->fetchall_arrayref([0])->[0]->[0];
        return 0 unless $row;

        return 1;
    }

    sub push {
        my ($self, $obj) = @_;
        my $vl = encode_json $obj;
        my $dbi = $self->{dbi};
        my $queue = $self->{table};

        $dbi->prepare_cached("
            INSERT INTO $queue ( value ) VALUES ( ? );
        ")->execute($vl);

    }

    sub pop {
        my ($self) = @_;
        my $dbi = $self->{dbi};
        my $queue = $self->{table};

        my $uuid = $dbi->prepare_cached("
            SELECT UUID();
        ");
        $uuid->execute();
        my $id = $uuid->fetchall_arrayref([0])->[0]->[0];

        my $setter = $dbi->prepare_cached("
            UPDATE $queue SET trans = ? WHERE trans IS NULL LIMIT 1;
        ");
        my $r = $setter->execute($id);
        return if $r == 0;

        my $fetch = $dbi->prepare_cached("
            SELECT value FROM $queue WHERE trans = ?;
        ");
        $fetch->execute($id);
        my $row = $fetch->fetchall_arrayref([0])->[0]->[0];
        return unless $row;

        my $obj = decode_json $row;
        $obj->{tid} = $id;

        return $obj;
    }

    sub done {
        my ($self, $obj, $status) = @_;
        my $dbi = $self->{dbi};
        my $queue = $self->{table};

        return unless $status == 1 or $status == 2;

        $dbi->prepare_cached("
            DELETE FROM $queue WHERE trans = ?;
        ")->execute($obj->{tid});

    }

    my $select = sub {
        my ($self, $filter) = @_;
        my $dbi = $self->{dbi};
        my $queue = $self->{table};

        my %rows;

        my $fetch = $dbi->prepare_cached("
            SELECT id, value FROM $queue WHERE trans $filter;
        ");
        $fetch->execute();

        return sub {
            my $row = $fetch->fetchrow_arrayref;
            return unless $row;
            return {
                id => $row->[0],
                value => decode_json $row->[1]
            };
        };
    };

    sub pending {
        my $self = shift;
        return $select->($self, "IS NULL");
    }

    sub sent {
        my $self = shift;
        return $select->($self, "IS NOT NULL");
    }

}

1;
