#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use utf8::all;
use Encode 'encode';
use warnings qw(all);

use FindBin qw($Bin);
use lib "$Bin/..";
use Ocr;

my ($img) = @ARGV;

my $phone = get_ocr($img);

say $phone;

my $ext = $img =~ /[.](\w+)$/ ? $1 : '';
my $out = $phone;
$out =~ s/[(]//g;
$out =~ s/[)]//g;
$out =~ s/[ ]/_/g;
`cp $img ${out}.${ext}`;
