
my $dbh;

sub insert_ad {
    my ($ad) = @_;

    unless ( $dbh ) { 
        $dbh = DBI->connect($db, $db_user, $db_pass);
        my $verifycmd = $dbh->prepare(qw(
            SELECT COUNT(*) 
            FROM information_schema.tables
            WHERE table_schema = 'yourdb' 
                AND table_name = 'testtable'
            LIMIT 1
        ));
        unless ( $verifycmd->execute() > 0 ) {
            my $createcmd = $dbh->prepare(qw(
                CREATE TABLE ad ( 
                    id INT PRIMARY KEY, 
                    product VARCHAR(256), 
                    city VARCHAR(256), 
                    user_name VARCHAR(256),
                    user_phone VARCHAR(256) 
                )
            ));
            $createcmd->execute();
        }  
    }

    my $cmd = $dbh->prepare(qw(
        INSERT INTO ad ( id, product, city, user_name, user_phone ) 
        VALUES ( ?, ?, ?, ?, ? )
    ));
    
    # Execute the query
    $cmd->execute(
        $ad->{id},
        $ad->{product},
        $ad->{city},
        $ad->{user_name},
        $ad->{user_phone},
    );

}
