#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Mojo::UserAgent;
use Mojo::UserAgent::CookieJar;
use Data::Dumper;

# Command line parameters
my ($product, $message) = @ARGV;

# FIFO queue
my @msgs = map { { 
    obj => { product => int($product), message => $message }, 
    cb => \&message_step1 
} } 0;

# Limit parallel connections to 4
my $max_conn = 4;

# User agent following up to 5 redirects
my $ua = Mojo::UserAgent
    ->new(max_redirects => 5);

$ua->cookie_jar(Mojo::UserAgent::CookieJar->new());

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

# Keep track of active connections
my $active = 0;

Mojo::IOLoop->recurring(
    0 => sub {
        for ($active + 1 .. $max_conn) {
            # Dequeue or halt if there are no active crawlers anymore
            return ($active or Mojo::IOLoop->stop())
                unless my $run = shift @msgs;
            # Fetch non-blocking just by adding
            # a callback and marking as active
            ++$active;
            $run->{cb}($run->{obj}, sub {
                # Deactivate
                --$active;
            }, 0);
        }
    }
);

# First we do the login synchronously
login();

# Start event loop if necessary
Mojo::IOLoop->start() unless Mojo::IOLoop->is_running;

sub login {

    # User information
    my $email = 'luiz_felipe_s@bol.com.br';
    my $password = '01677610';

    my $tx;

    # Get the main page cookies if any
    $tx = $ua->get(
        'http://www.olx.com.br' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'});

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Get the login page cookies if any
    $tx = $ua->get(
        'https://www3.olx.com.br/account/form_login/' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'});

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Do the login
    $tx = $ua->post(
        'https://www3.olx.com.br/account/do_login' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        form => {
            email => $email,
            password => $password,
            entrar => 'Entrar'
        });

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Our cookie_jar will contain the needed cookie now
    # say Dumper($ua->cookie_jar->all);

    return 1;
}

sub callback {
    my ($obj, $done, $prevtx, $next) = @_;
    return sub {
        my (undef, $tx) = @_;

        say Dumper($tx);

        # Parse only OK responses
        if (not $tx->res->is_status_class(200)) {
            $done->();
            return;
        }

        # Specific callback
        $next->($obj, $done, $tx);

        return;
    };
}

#used to start async
sub message_step1 {
    my ($obj, $done, $tx) = @_;

    say "step1";

    # Get the chat page option, just to emulate
    $ua->options(
        'https://www3.olx.com.br/chats' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        callback(@_, \&message_step2));

    return;
}

sub message_step2 {
    my ($obj, $done, $tx) = @_;

    say "step2";

    # Get the chat page
    $ua->post(
        'https://www3.olx.com.br/chats' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        json => {
            "listId" => $obj->{product}
        } =>
        callback(@_, \&message_step3));

    return;
}

#used to start async
sub message_step3 {
    my ($obj, $done, $tx) = @_;

    my $id = $tx->res->json->{data}->{id};

    say $tx->res->content->asset->slurp();
    say "step3";

    $obj->{chat} = $id;

    # Get the chat page option, just to emulate
    $ua->options(
        "https://www3.olx.com.br/chats/$id/messages" => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        callback(@_, \&message_step4));

    return;
}

sub message_step4 {
    my ($obj, $done, $tx) = @_;

    my $id = $obj->{chat};

    say "step4";

    # Get the chat page
    $ua->get(
        "https://www3.olx.com.br/chats/$id/messages" => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        callback(@_, \&message_step5));

    return;
}

sub message_step5 {
    my ($obj, $done, $tx) = @_;

    my $id = $obj->{chat};

    say $tx->res->content->asset->slurp();
    say "step5";

    # Post the message
    $ua->post(
        "https://www3.olx.com.br/chats/$id/messages" => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        json => {
            "type" => "TEXT",
            "text" => $message
        } =>
        callback(@_, \&message_step6));

    return;
}

sub message_step6 {
    my ($obj, $done, $tx) = @_;

    say $tx->res->content->asset->slurp();
    say "done";

    # If we got there then the message was sent
    # Signal we're done
    $done->();

    return;
}
