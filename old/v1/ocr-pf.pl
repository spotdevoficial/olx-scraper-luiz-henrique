#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use OCR::PerfectCR;
use GD;

use Mojo::UserAgent;

my $ua = Mojo::UserAgent
    ->new(max_redirects => 5);

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

my $img = 'http://static.bn-static.com/pg/0e7ScAAph5+RLrYp1Uwoe6WVSRQ7NUGKwd2Qnxn4nfg==.gif';
my $link = Mojo::URL->new($img);

Mojo::IOLoop->recurring(
    0 => sub {
        ocr_image();        
    }
);

# Start event loop if necessary
Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

sub ocr_image {
    $ua->get($link => sub {
        my (undef, $tx) = @_;

        # Parse only OK HTML responses
        return if not $tx->res->is_status_class(200);

        # Grab the image data        
        my $img_data = $tx->res->content->asset->slurp();

        # Load image
        my $image = GD::Image->new($img_data);
        my $recognizer = OCR::PerfectCR->new();
        $recognizer->load_charmap_file("charmap");
        my $string = $recognizer->recognize($image);
        $recognizer->save_charmap_file("charmap");
        say $string;

        Mojo::IOLoop->stop;
    });
}