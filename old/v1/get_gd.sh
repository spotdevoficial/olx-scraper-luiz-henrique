#!/bin/bash
wget http://search.cpan.org/CPAN/authors/id/L/LD/LDS/GD-2.56.tar.gz
  -or-
curl -L http://search.cpan.org/CPAN/authors/id/L/LD/LDS/GD-2.56.tar.gz >GD-2.56.tar.gz

tar xvzf GD-2.56.tar.gz
cd GD-2.56
perl -i~ -pE'say "Getopt::Long::Configure(qw( pass_through ));" if /GetOptions/;' Build.PL
/usr/bin/perl Build.PL --installdirs site
sudo ./Build.PL installdeps
./Build.PL make
./Build.PL test
sudo ./Build.PL install
