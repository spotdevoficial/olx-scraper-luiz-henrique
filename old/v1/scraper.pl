#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use String::Util qw(trim);
use Image::OCR::Tesseract qw(get_ocr);
use Mojo::UserAgent;
use File::Temp;

# Command line parameters
my ($filter_state, $filter_category, $filter_page_min, $filter_page_max) = @ARGV;

$filter_state = '.+' unless $filter_state;
$filter_category = '.+' unless $filter_category;
$filter_page_min = 0 unless $filter_page_min;
$filter_page_max = 99999999 unless $filter_page_max;

say $filter_state;
say $filter_category;
say $filter_page_min;
say $filter_page_max;

# FIFO queue
my @urls = map { { url => Mojo::URL->new($_), cb => \&parse_html_step1 } } 
qw(
    http://www.olx.com.br/
);

# Limit parallel connections to 4
my $max_conn = 4;

# User agent following up to 5 redirects
my $ua = Mojo::UserAgent
    ->new(max_redirects => 5);

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

# Keep track of active connections
my $active = 0;

Mojo::IOLoop->recurring(
    0 => sub {
        for ($active + 1 .. $max_conn) {
            # Dequeue or halt if there are no active crawlers anymore
            return ($active or Mojo::IOLoop->stop())
                unless my $run = shift @urls;
            # Fetch non-blocking just by adding
            # a callback and marking as active
            ++$active;
            $ua->get($run->{url} => sub { get_callback(@_, $run->{cb}); });
        }
    }
);

# Start event loop if necessary
Mojo::IOLoop->start() unless Mojo::IOLoop->is_running;

sub get_callback {
    my (undef, $tx, $cb) = @_;

    # Deactivate
    --$active;

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Request URL
    my $url = $tx->req->url;

    # Specific callback
    $cb->($url, $tx);

    return;
}

sub parse_html_links {
    my ($url, $tx, $section, $elem, $filter, $callback) = @_;

    # Extract list of links
    my $sec = $tx->res->dom($section)->first;

    # Extract and enqueue URLs
    for my $e ($sec->find($elem)->each) {

        # ignore javascript links
        my $href = $e->{href};
        next unless $href;

        # Validate href attribute
        my $link = Mojo::URL->new($href);
        next if 'Mojo::URL' ne ref $link;

        # "normalize" link
        $link = $link->to_abs($tx->req->url)->fragment(undef);
        next unless grep { $link->protocol eq $_ } qw(http https);

        # Filter
        next unless grep { $link->to_string() =~ $_ } $filter;

        ## Access every link only once
        #state $uniq = {};
        #++$uniq->{$url->to_string()};
        #next if ++$uniq->{$link->to_string()} > 1;

        # Process the link
        $callback->($link);
    }

    return;
}

sub parse_html_step1 {
    my ($url, $tx) = @_;

    parse_html_links($url, $tx, 
        'div[class~=section_state]',
        'a[class~=link]',
        $filter_state,
        sub {
            my ($link) = @_;
            # Don't visit our parent
            next if $link->host eq $url->host;
            # Push the link
            push @urls, { url => $link, cb => \&parse_html_step2 };
            say " -> $link";
        });

    return;
}

sub parse_html_step2 {
    my ($url, $tx) = @_;

    parse_html_links($url, $tx, 
        'div[class~=search-category]',
        'a[class~=link]',
        $filter_category,
        sub {
            my ($link) = @_;
            # Push the link
            push @urls, { url => $link, cb => \&parse_html_step3 };
            say "    -> $link";
        });       

    return;
}

sub parse_html_step3 {
    my ($url, $tx) = @_;

    my $min = $filter_page_min;
    my $max = 0;
    my $reflink;

    parse_html_links($url, $tx, 
        'div[class~=module_pagination]',
        'a[class~=link]',
        '.+',
        sub {
            my ($link) = @_;
            my $pg = $link =~ /(\d+)$/ ? int($1) : 0;
            if ( $pg > $max ) { $max = $pg; $reflink = $link; }
        });       

    if ( $max > $filter_page_max ) { $max = $filter_page_max; }

    $reflink = $reflink =~ /^([^\d]+)\d+$/ ? $1 : '';

    # Push the links
    for my $page ( $min .. $max ) {
        my $link = $reflink . $page;
        push @urls, { url => $link, cb => \&parse_html_step4 };
        say "        -> $link";
    }

    return;
}
 
sub parse_html_step4 {
    my ($url, $tx) = @_;

    parse_html_links($url, $tx, 
        'div[class~=section_OLXad-list]',
        'a[class~=OLXad-list-link]',
        '.+',
        sub {
            my ($link) = @_;
            # Push the link
            push @urls, { url => $link, cb => \&parse_html_step5 };
            say "            -> $link";
        });   

    return;
}

sub parse_html_step5 {
    my ($url, $tx) = @_;
    
    say "            ? $url";

    # Extract ad data
    my $ad = $tx->res->dom('div[class~=OLXad-id]')->first;
    my $id = $ad->find('*[class~=description]')->first;

    # Filter ad id
    my $ad_id = $id->all_text() if $id;

    # Extract ad name
    my $title = $tx->res->dom('*[class~=OLXad-title]')->first;

    # Filter product
    my $ad_product = $title->all_text() if $title;

    # Extract ad location
    my $loc = $tx->res->dom('div[class~=OLXad-location]')->first;
    my $city;
    for my $e ($loc->find('*[class~=item]')->each) {
        my $term = $e->find('*[class~=term]')->first;
        next unless $term and $term->all_text() =~ /munic.pio/i;
        $city = $e->find('*[class~=description]')->first;
    }

    # Filter city
    my $ad_city = $city->all_text() if $loc;

    # Extract user data
    my $user = $tx->res->dom('div[class~=section_OLXad-user-info]')->first;
    my $name = $user->find('li[class~=owner]')->first;
    my $phone = $user->find('li[class~=phone]')->first;
    my $imgphone = $phone->find('img[class~=number]')->first if $phone;

    # Filter name
    my $user_name = $name->all_text() if $name;
                
    # Filter phone
    my $link = Mojo::URL->new($imgphone->{src}) if $imgphone;
    $link = $link->to_abs($tx->req->url)->fragment(undef)
        unless not $link or 'Mojo::URL' ne ref $link;
    my $user_phone = ocr_image($link->to_string()) if $link;

    # Save Ad
    say $ad_id;
    say $ad_product;
    say $ad_city;
    say $user_name;
    say $user_phone;

    return;
}

sub ocr_image {
    my ($link) = @_;

    # Get
    my $tx = $ua->get($link);

    # Parse only OK HTML responses
    return '' if not $tx->res->is_status_class(200);

    # Grab the image data        
    my $image = tmpnam();
    $tx->res->content->asset->move_to($image);

    # Scan image
    my $string = get_ocr($image);

    # Close
    unlink $image;

    return trim $string;
}

