#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use String::Util qw(trim);
use Image::OCR::Tesseract qw(get_ocr);
use Mojo::UserAgent;
use File::Temp;

my $ua = Mojo::UserAgent
    ->new(max_redirects => 5);

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

Mojo::IOLoop->recurring(
    0 => sub {
        my $url = 'http://static.bn-static.com/pg/0e7ScAAph5+RLrYp1Uwoe6WVSRQ7NUGKwd2Qnxn4nfg==.gif';
        ocr_image($url);
    }
);

# Start event loop if necessary
Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

sub ocr_image {
    my ($imglink) = @_;
    my $link = Mojo::URL->new($imglink);
    $ua->get($link => sub {
        my (undef, $tx) = @_;

        # Parse only OK HTML responses
        return if not $tx->res->is_status_class(200);

        # Grab the image data        
        my $image = tmpnam();
        $tx->res->content->asset->move_to($image);

        # Scan image
        my $string = get_ocr($image);
        say trim $string;

        # Close
        unlink $image;

        Mojo::IOLoop->stop;
    });
}
