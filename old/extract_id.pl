#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json);
use FindBin qw($Bin);
use lib "$Bin/..";
use Queue;
use config;

# Usage:
# Set all requests that don't have response as not processed
# perl reissue.pl

# Consts
my $step_max = 6;

# Database
my $db_host = config()->{host};
my $db_user = config()->{user};
my $db_pass = config()->{pass};

my $iterator;

my $messages = Queue->create({
    host => $db_host,
    db => 'message',
    table => 'queue',
    user => $db_user,
    pass => $db_pass
});

my $msg = Queue->create({ 
    host => $db_host, 
    db => 'message', 
    table => 'ix_queue',
    user => $db_user, 
    pass => $db_pass
});

#index message.queue/product on tmp.message.id with url
$iterator = $messages->pending();
while (my $row = $iterator->()) {
    $msg->add($row->{value}->{obj}->{product}, $row->{value}->{obj}->{url});
}
