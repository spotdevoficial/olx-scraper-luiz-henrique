create table tmp.message_url ( id int, url VARCHAR(1024) ) engine=INNODB;
create index message_url_url on tmp.message_url(url);
insert into tmp.message_url select id, LEFT(value,LENGTH(value)-LENGTH(SUBSTRING_INDEX(value,'-',-1))-1) as url from tmp.message;

create table tmp.message_url_g ( url VARCHAR(1024) ) engine=INNODB;
create index message_url_g_url on tmp.message_url_g(url);
insert into tmp.message_url_g select url from tmp.message_url group by url having count(*) > 2;

select * from tmp.message_url m inner join tmp.message_url_g g on m.url = g.url;

create table tmp.message_g ( value VARCHAR(1024) ) engine=INNODB;
create index message_g_url on tmp.message_g(value);
insert into tmp.message_g select value from tmp.message group by value having count(*) > 2;

select * from tmp.message m inner join tmp.message_g g on m.value = g.value;

