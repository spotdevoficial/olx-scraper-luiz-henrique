#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use warnings qw(all);
use utf8;
use utf8::all;
use Encode 'encode';

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json from_json);
use Queue;

# Database
my $db_host = 'localhost';
my $db_user = 'root';
my $db_pass = '123';

my $ads = Queue->create({ 
    host => $db_host, 
    db => 'scraper', 
    table => 'ads',
    user => $db_user, 
    pass => $db_pass
});

try { $ads->{dbi}->prepare("
    CREATE TABLE ads2 ( 
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        product VARCHAR(256) NULL,
        city VARCHAR(256) NULL,
        name VARCHAR(256) NULL,
        phone VARCHAR(256) NULL )
        ENGINE = innodb;
")->execute(); };

# $ads->{dbi}->{'mysql_enable_utf8'} = 1;

# find all the responses that we got and save their urls
my $iterator = $ads->pending();
while (my $row = $iterator->()) {
    my $id = $row->{value}->{id};
    my $product = $row->{value}->{product};
    my $city = $row->{value}->{city};
    my $user_name = $row->{value}->{user_name};
    my $user_phone = $row->{value}->{user_phone};

    $product = encode('ISO-8859-1', $product);
    $city = encode('ISO-8859-1', $city);
    $user_name = encode('ISO-8859-1', $user_name);
    $user_phone = encode('ISO-8859-1', $user_phone);

    $ads->{dbi}->prepare_cached("
        INSERT INTO ads2 ( id, product, city, name, phone ) VALUES ( ?, ?, ?, ?, ? );
    ")->execute( $id, $product, $city, $user_name, $user_phone);
}
