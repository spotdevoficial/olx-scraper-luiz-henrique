#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use warnings qw(all);
use utf8;
use utf8::all;
use Encode 'encode';

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json from_json);
use Queue;
use config;

# Database
my $db_host = config()->{host};
my $db_user = config()->{user};
my $db_pass = config()->{pass};

my $messages = Queue->create({ 
    host => $db_host, 
    db => 'message', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

# find all the responses that we got and save their urls
my $iterator = $messages->pending();
while (my $row = $iterator->()) {

    my $id = $row->{value}->{obj}->{product};
    my $url = $row->{value}->{obj}->{url};
    my $state = $url =~ /^[^:]+[:]\/\/(.+)[.]olx[^\/]+\// ? $1 : '';

    $messages->{dbi}->prepare_cached("
        UPDATE ads SET state = ? WHERE id = ?;
    ")->execute( $state, $id );

}
