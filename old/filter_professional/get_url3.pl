#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json);
use Queue;

# Database
my $db_host = 'localhost';
my $db_user = 'root';
my $db_pass = '123';

my $ads = Queue->create({ 
    host => $db_host, 
    db => 'scraper', 
    table => 'ads',
    user => $db_user, 
    pass => $db_pass
});

my $messages = Queue->create({ 
    host => $db_host, 
    db => 'message', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

#Target database
my $filter = Queue->create({ 
    host => $db_host, 
    db => 'url', 
    table => 'ads',
    user => $db_user, 
    pass => $db_pass
});

try { $filter->{dbi}->prepare("
    alter table ads modify column value varchar(4096) not null;
")->execute(); };
try { $filter->{dbi}->prepare("
    create index ads_value on ads(value);
")->execute(); };

my $iterator;

my $ads_id = {};

# find all the ads that we got and save their row ids
$iterator = $ads->pending();
while (my $row = $iterator->()) {
    $ads_id->{$row->{value}->{id}} = $row->{id};
}

# find all the messages for the ads that we will send and save their urls
$iterator = $messages->pending();
while (my $row = $iterator->()) {
    my $url = ref $row->{value}->{obj}->{filter} eq 'HASH' ?
        $row->{value}->{obj}->{url} :
        $row->{value}->{obj}->{filter};    
    my $id = $row->{value}->{obj}->{product};
    $filter->add($ads_id->{$id}, $url); 
}
