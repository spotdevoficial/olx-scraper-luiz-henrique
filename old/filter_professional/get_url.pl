#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json);
use Queue;

# Database
my $db_host = 'localhost';
my $db_user = 'root';
my $db_pass = '123';

my $urls = Queue->create({ 
    host => $db_host, 
    db => 'scraper', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

my $messages = Queue->create({ 
    host => $db_host, 
    db => 'message', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

#Target database
my $request = Queue->create({ 
    host => $db_host, 
    db => 'url', 
    table => 'request',
    user => $db_user, 
    pass => $db_pass
});

my $response = Queue->create({ 
    host => $db_host, 
    db => 'url', 
    table => 'response',
    user => $db_user, 
    pass => $db_pass
});

try { $request->{dbi}->prepare("
    alter table request  modify column value varchar(4096) not null;
")->execute(); };
try { $response->{dbi}->prepare("
    alter table response modify column value varchar(4096) not null;
")->execute(); };
try { $request->{dbi}->prepare("
    create index request_value  on request(value);
")->execute(); };
try { $response->{dbi}->prepare("
    create index response_value on response(value);
")->execute(); };

my $iterator;

# find all the requests that were made and save their urls
$iterator = $urls->sent();
while (my $row = $iterator->()) {
    next unless $row->{value}->{step} == 5;
    $request->add($row->{id}, $row->{value}->{url});
}

# find all the responses that we got and save their urls
$iterator = $messages->pending();
while (my $row = $iterator->()) {
    my $url = ref $row->{value}->{obj}->{filter} eq 'HASH' ?
        $row->{value}->{obj}->{url} :
        $row->{value}->{obj}->{filter};
    $response->add($row->{id}, $url); 
}
