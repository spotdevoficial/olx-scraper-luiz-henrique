#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use String::Util qw(trim);
use Image::OCR::Tesseract qw(get_ocr);
use Mojo::UserAgent;
use Mojo::JSON qw(decode_json encode_json);
use File::Temp;
use Data::Dumper;
use DBD::mysql;
use Queue;

# Usage:
# start scavenging everything 
# perl scraper.pl start
# start scavenging with some filters
# perl scraper.pl start [state] [category] [page_min] [page_max]     
# don't run the main loop, just enqueue new filters on the database
# perl scraper.pl queue [state] [category] [page_min] [page_max] 
# continue scavenging until queue empties, used to run more processes
# perl scraper.pl       
# continue scavenging forever, used to run more processes
# perl scraper.pl loop

# Command line parameters
my ($mode, $filter_state, $filter_category, $filter_page_min, $filter_page_max) = @ARGV;

$mode = '' unless $mode;
$filter_state = '.+' unless $filter_state;
$filter_category = '.+' unless $filter_category;
$filter_page_min = 0 unless $filter_page_min;
$filter_page_max = 99999999 unless $filter_page_max;

my $filter = {
    state => $filter_state,
    category => $filter_category,
    page_min => $filter_page_min,
    page_max => $filter_page_max
};

# Database
my $db_host = 'localhost';
my $db_user = 'root';
my $db_pass = '123';

my $urls = Queue->create({ 
    host => $db_host, 
    db => 'scraper2', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

my $filtered = Queue->create({ 
    host => $db_host, 
    db => 'filtered', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

# Push initial link
if ( $mode eq 'start' or $mode eq 'queue' ) {
    $urls->push({ url => 'http://www.olx.com.br/', filter => $filter, step => 1 });
    exit if $mode eq 'queue';
}

# Service mode
my $forever = $mode eq 'loop';

# User agent following up to 5 redirects
my $ua = Mojo::UserAgent
    ->new(max_redirects => 5);

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

# Limit parallel connections
my $max_conn = 4;

# Keep track of active connections
my $active = 0;

# Steps
my $steps = { 
    1 => \&parse_html_step1,
    2 => \&parse_html_step2,
    3 => \&parse_html_step3,
    4 => \&parse_html_step4,
    5 => \&parse_html_step5,
};

# Main Loop
Mojo::IOLoop->recurring(
    0 => sub {
        for ($active + 1 .. $max_conn) {
            # Dequeue 
            last unless my $run = $urls->pop();
            # Fetch non-blocking just by adding
            # a callback and marking as active
            ++$active;
            $ua->get($run->{url} => sub { 
                get_callback(@_, $steps->{$run->{step}}, $run->{filter}); 
            });
        }
        # or halt if there are no active crawlers anymore
        Mojo::IOLoop->stop() if not $active and not $forever;
    }
);

# Start event loop if necessary
Mojo::IOLoop->start() unless Mojo::IOLoop->is_running;

sub get_callback {
    my (undef, $tx, $cb, $filter) = @_;

    # Deactivate
    --$active;

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Request URL
    my $url = $tx->req->url;

    # Access every link only once
    state $uniq = {};
    return if $uniq->{$url->to_string()};
    ++$uniq->{$url->to_string()};

    # Specific callback
    $cb->($url, $filter, $tx);

    return;
}

sub parse_html_links {
    my ($url, $tx, $section, $elem, $filter, $callback) = @_;

    # Extract list of links
    my $sec = $tx->res->dom($section)->first;

    # Extract and enqueue URLs
    for my $e ($sec->find($elem)->each) {

        # ignore javascript links
        my $href = $e->{href};
        next unless $href;

        # Validate href attribute
        my $link = Mojo::URL->new($href);
        next if 'Mojo::URL' ne ref $link;

        # "normalize" link
        $link = $link->to_abs($tx->req->url)->fragment(undef);
        next unless grep { $link->protocol eq $_ } qw(http https);

        # Filter
        next unless grep { $link->to_string() =~ $_ } $filter;

        # Process the link
        $callback->($link);
    }

    return;
}

sub parse_html_step1 {
    my ($url, $filter, $tx) = @_;

    parse_html_links($url, $tx, 
        'div[class~=section_state]',
        'a[class~=link]',
        $filter->{state},
        sub {
            my ($link) = @_;
            # Don't visit our parent
            next if $link->host eq $url->host;
            # Push the link
            $urls->push({ url => $link, filter => $filter, step => 2 });
            say " -> $link";
        });

    return;
}

sub parse_html_step2 {
    my ($url, $filter, $tx) = @_;

    parse_html_links($url, $tx, 
        'div[class~=search-category]',
        'a[class~=link]',
        $filter->{category},
        sub {
            my ($link) = @_;
            # Filter link
            my $url = Mojo::URL->new($link->to_string() . '?f=c');
            # Push the link
            $urls->push({ url => $url, filter => $filter, step => 3 });
            say "    -> $link";
        });       

    return;
}

sub parse_html_step3 {
    my ($url, $filter, $tx) = @_;

    my $min = $filter->{page_min};
    my $max = 0;
    my $reflink;

    parse_html_links($url, $tx, 
        'div[class~=module_pagination]',
        'a[class~=link]',
        '.+',
        sub {
            my ($link) = @_;
            my $pg = $link =~ /(\d+)$/ ? int($1) : 0;
            if ( $pg > $max ) { $max = $pg; $reflink = $link; }
        });       

    if ( $max > $filter->{page_max} ) { $max = $filter->{page_max}; }

    $reflink = $reflink =~ /^([^\d]+)\d+$/ ? $1 : '';

    # Push the links
    for my $page ( $min .. $max ) {
        my $link = $reflink . $page;
        $urls->push({ url => $link, filter => $filter, step => 4 });
        say "        -> $link";
    }

    return;
}
 
sub parse_html_step4 {
    my ($url, $filter, $tx) = @_;

    parse_html_links($url, $tx, 
        'div[class~=section_OLXad-list]',
        'a[class~=OLXad-list-link]',
        '.+',
        sub {
            my ($link) = @_;
            # Push the link
            $filtered->push({ url => $link, filter => $filter, step => 5 });
            say "            -> $link";
        });   

    return;
}
