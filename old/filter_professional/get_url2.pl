#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Try::Tiny;
use Data::Dumper;
use Mojo::JSON qw(decode_json encode_json);
use Queue;

# Database
my $db_host = 'localhost';
my $db_user = 'root';
my $db_pass = '123';

my $urls = Queue->create({ 
    host => $db_host, 
    db => 'filtered', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

#Target database
my $filter = Queue->create({ 
    host => $db_host, 
    db => 'url', 
    table => 'filter',
    user => $db_user, 
    pass => $db_pass
});

try { $filter->{dbi}->prepare("
    alter table filter modify column value varchar(4096) not null;
")->execute(); };
try { $filter->{dbi}->prepare("
    create index filter_value on filter(value);
")->execute(); };

my $iterator;

# find all the urls that we got and save their urls unserialized
$iterator = $urls->pending();
while (my $row = $iterator->()) {
    my $url = ref $row->{value}->{filter} eq 'HASH' ?
        $row->{value}->{url} :
        $row->{value}->{filter};
    $filter->add($row->{id}, $url); 
}
