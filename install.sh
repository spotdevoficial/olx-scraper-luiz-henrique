apt-get update
apt-get upgrade
apt-get install -f git
apt-get install -f perl
apt-get install -f make
apt-get install -f gcc

apt-get install -f iotop
apt-get install -f nload


apt-get install -f libwww-perl
apt-get install -f libmojolicious-perl
apt-get install -f libutf8-all-perl
apt-get install -f libsharyanto-string-util-perl 
apt-get install -f libyaml-libyaml-perl
apt-get install -f libyaml-perl
apt-get install -f libwww-perl 
apt-get install -f libdbi-perl
apt-get install -f libmysqlclient-dev
apt-get install -f libdbd-mysql-perl
apt-get install -f libgd-gd2-perl
apt-get install -f libdevel-dprof-perl
apt-get install -f libev-perl
apt-get install -f tesseract-ocr

cpan Thread:Queue
cpan Sub::Uplevel
cpan Test::Fatal
cpan utf8
cpan utf8::all
cpan Carp 
cpan HTTP::Tiny
cpan LWP
cpan Path::Tiny
cpan Try::Tiny
cpan String::Util
cpan Mojo::UserAgent
cpan Mojo::JSON
cpan File::Temp 
cpan Data::Dumper
cpan Data::GUID
cpan DBI
cpan DBD::mysql 
cpan Devel::NYTProf
