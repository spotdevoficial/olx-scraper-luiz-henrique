#!/usr/bin/env perl
use 5.010;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

use Mojo::UserAgent;
use Mojo::UserAgent::CookieJar;
use Mojo::JSON qw(decode_json encode_json);
use Data::Dumper;
use DBD::mysql;
use Queue;
use config;

# Usage:
# start sending a message
# perl message.pl start [ad_id] [message]
# don't run the main loop, just enqueue new messages on the database
# perl message.pl queue [ad_id] [message]
# continue sending until queue empties
# perl message.pl      
# continue sending forever, used to run more processes
# perl message.pl loop

# Command line parameters
my ($mode, $product, $message) = @ARGV;

$mode = '' unless $mode;
$product = '' unless $product;
$message = '' unless $message;

# Database
my $db_host = config()->{host};
my $db_user = config()->{user};
my $db_pass = config()->{pass};

my $messages = Queue->create({ 
    host => $db_host, 
    db => 'message', 
    table => 'queue',
    user => $db_user, 
    pass => $db_pass
});

my $sent = Queue->create({ 
    host => $db_host, 
    db => 'scraper', 
    table => 'sent',
    user => $db_user, 
    pass => $db_pass
});

# Push initial message
if ( $mode eq 'start' or $mode eq 'queue' ) {
    $messages->push({ 
        obj => { product => int($product), message => $message }, 
        step => 1 
    });
    exit if $mode eq 'queue';
}

# Service mode
my $forever = $mode eq 'loop';

# User agent following up to 5 redirects
my $ua = Mojo::UserAgent
    ->new(max_redirects => 5);

$ua->cookie_jar(Mojo::UserAgent::CookieJar->new());

$ua->transactor->name('Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');

# Limit parallel connections
my $max_conn = 4;

# Keep track of active connections
my $active = 0;

# Steps
my $steps = { 
    1 => \&message_step1
};

# Main Loop
Mojo::IOLoop->recurring(
    1 => sub {
        for ($active + 1 .. $max_conn) {
            # Dequeue 
            last unless my $run = $messages->pop();
            # Fetch non-blocking just by adding
            # a callback and marking as active
            ++$active;
            $steps->{1}($run->{obj}, sub {
                # Mark as finished
                $messages->done($run->{transaction});
                # Deactivate
                --$active;
            }, 0);
        }
        # or halt if there are no active crawlers anymore
        Mojo::IOLoop->stop() if not $active and not $forever;
    }
);

# First we do the login synchronously
login();

# Start event loop if necessary
Mojo::IOLoop->start() unless Mojo::IOLoop->is_running;

sub login {

    # User information
    my $email = 'e@mail.com';
    my $password = '123';

    my $tx;

    # Get the main page cookies if any
    $tx = $ua->get(
        'http://www.olx.com.br' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'});

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Get the login page cookies if any
    $tx = $ua->get(
        'https://www3.olx.com.br/account/form_login/' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'});

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Do the login
    $tx = $ua->post(
        'https://www3.olx.com.br/account/do_login' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        form => {
            email => $email,
            password => $password,
            entrar => 'Entrar'
        });

    # Parse only OK HTML responses
    return
        if not $tx->res->is_status_class(200)
        or $tx->res->headers->content_type !~ m{^text/html\b}ix;

    # Our cookie_jar will contain the needed cookie now
    # say Dumper($ua->cookie_jar->all);

    return 1;
}

sub callback {
    my ($obj, $done, $prevtx, $next) = @_;
    return sub {
        my (undef, $tx) = @_;

        # Parse only OK responses
        if (not $tx->res->is_status_class(200)) {
            $done->();
            return;
        }

        # Specific callback
        $next->($obj, $done, $tx);

        return;
    };
}

#used to start async
sub message_step1 {
    my ($obj, $done, $tx) = @_;

    #say "step1 " . $obj->{product};

    # Get the chat page option, just to emulate
    $ua->options(
        'https://www3.olx.com.br/chats' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        callback(@_, \&message_step2));

    return;
}

sub message_step2 {
    my ($obj, $done, $tx) = @_;

    #say "step2 " . $obj->{product};

    # Get the chat page
    $ua->post(
        'https://www3.olx.com.br/chats' => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        json => {
            "listId" => $obj->{product}
        } =>
        callback(@_, \&message_step3));

    return;
}

#used to start async
sub message_step3 {
    my ($obj, $done, $tx) = @_;

    my $id = $tx->res->json->{data}->{id};

    #say $tx->res->content->asset->slurp();
    #say "step3 " . $obj->{product};

    $obj->{chat} = $id;

    # Get the chat page option, just to emulate
    $ua->options(
        "https://www3.olx.com.br/chats/$id/messages" => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        callback(@_, \&message_step4));

    return;
}

sub message_step4 {
    my ($obj, $done, $tx) = @_;

    my $id = $obj->{chat};

    #say "step4 " . $obj->{product};

    # Get the chat page
    $ua->get(
        "https://www3.olx.com.br/chats/$id/messages" => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        callback(@_, \&message_step5));

    return;
}

sub message_step5 {
    my ($obj, $done, $tx) = @_;

    my $id = $obj->{chat};

    #say $tx->res->content->asset->slurp();
    #say "step5 " . $obj->{product};

    # Post the message
    $ua->post(
        "https://www3.olx.com.br/chats/$id/messages" => {
            'Accept' => '*/*',
            'Accept-Language' => 'en-US,en;q=0.5'} =>
        json => {
            "type" => "TEXT",
            "text" => $obj->{message}
        } =>
        callback(@_, \&message_step6));

    return;
}

sub message_step6 {
    my ($obj, $done, $tx) = @_;

    #say $tx->res->content->asset->slurp();
    #say "done " . $obj->{product};

    # save the record
    $sent->push($obj);

    # If we got there then the message was sent
    # Signal we're done
    $done->();

    return;
}
