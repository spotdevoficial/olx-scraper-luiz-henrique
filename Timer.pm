#!/usr/bin/env perl
package Timer {
    use 5.016;
    use open qw(:locale);
    use strict;
    use utf8;
    use warnings qw(all);

    use Time::HiRes qw(gettimeofday tv_interval);
    use Data::Dumper;

    sub create {
        my $class = shift;
        my $self = bless {}, $class;
        my ($p) = @_; #params from constructor

        my @stack;
        $self->{stack} = \@stack;

        #return new instance (bless is like Lua's metatables)
        return $self;
    }

    sub measure {
        my ($self, $name) = @_;
        my $timer = [gettimeofday()];
        my @stack = @{ $self->{stack} };
        push @stack, $name => $timer;
        $self->{stack} = \@stack;
    }

    sub stop {
        my ($self) = @_;
        my @stack = @{ $self->{stack} };
        my $timer = pop @stack;
        my $name = pop @stack;
        my $time = tv_interval($timer);
        $self->{stack} = \@stack;
        $time += $self->{report}->{$name} 
            if $self->{report}->{$name};
        $self->{report}->{$name} = $time;
    }

    sub report {
        my ($self) = @_;
        my $rpt = $self->{report};
        for my $k (sort keys %$rpt) {
            my $time = $rpt->{$k};
            say $k . "\t" . sprintf("%.10f", $time);
        }
        say "---------------------------------------------";
    }

}

1;
