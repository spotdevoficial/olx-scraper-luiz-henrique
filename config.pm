#!/usr/bin/env perl
use 5.016;
use open qw(:locale);
use strict;
use utf8;
use warnings qw(all);

# Database
sub config {

    my $filename = 'database.cnf';
    open(my $fh, '<:encoding(UTF-8)', $filename)
        or die "Could not open file '$filename' $!";

    my $file_content = do { local $/; <$fh> };

    close($fh);

    my $take = sub {
        my $key = shift;
        return $file_content =~ /^${key}[ \t]+(.+)$/m ? $1 : "";
    };

    return {
        host => $take->('host'),
        user => $take->('user'),
        pass => $take->('pass')
    };

}

1;
