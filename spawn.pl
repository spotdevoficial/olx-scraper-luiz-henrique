#!/usr/bin/env perl
use 5.016;
use warnings qw(all);

# you can use more workers, but they start competing
# for the internet connection, there's a limit
my $workers = 8 - int(`ps -a | grep perl | wc -l`) + 1;
say "Spawning " . $workers . " processes";
for my $i ( 1 .. $workers ) {
    system('LIBEV_FLAGS=4 ./scraper.pl loop &');
    system('sleep 1');
}
